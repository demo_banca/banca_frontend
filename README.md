# Demo Banca (Frontend)

## Technologies

* HTML
* HTML 5
* JQuery 2.2.0
* Ajax
* Bootstrap 3.3.6
* Bootstrap-combobox 1.1.6
* Bootstrap-datepicker 1.6.0
* Bootstrap-notify 3.1.5
* Bootstrap-select 1.12.1
* Select 1.1.0
* FormValidation 0.7.2
* CSS
* SCSS

## Authors

* **Eng. Diego Huamanchahua**

## Image

![Demo Frontend](public_html/img/screenshot-frontend.png)