/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var API = "http://127.0.0.1:9001/";

var METHOD_POST = "POST";
var METHOD_GET = "GET";
var METHOD_PATCH = "PATCH";
var METHOD_PUT = "PUT";
var METHOD_DELETE = "DELETE";

var NOTIFICATION_INFO = "info";
var NOTIFICATION_DANGER = "danger";
var NOTIFICATION_SUCCESS = "success";
var NOTIFICATION_WARNING = "warning";
