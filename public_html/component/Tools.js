/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function notification(type, msn) {
    $.notify({
        message: msn
    }, {
        allow_dismiss: true,
        type: type, /* danger, info, success, warning */
        placement: {
            from: "bottom",
            align: "center"
        }
    });

    setTimeout(function () {
        $.notifyClose();
    }, 3000);
}
