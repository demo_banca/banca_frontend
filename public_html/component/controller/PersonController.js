/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function createNewPerson(person) {
    return $.ajax({
        type: METHOD_POST,
        url: API + "persons",
        data: JSON.stringify(person),
        contentType: "application/json",
        dataType: "json"
    });
}
