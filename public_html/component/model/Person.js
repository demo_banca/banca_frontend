/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Person = function () {

};

Person.prototype.setName = function (name) {
    this.name = name;
};

Person.prototype.getName = function () {
    return this.name;
};

Person.prototype.setBirthdate = function (birthdate) {
    this.birthdate = birthdate;
};

Person.prototype.getBirthdate = function () {
    return this.birthdate;
};

Person.prototype.setDepartment = function (department) {
    this.department = department;
};

Person.prototype.getDepartment = function () {
    return this.department;
};

Person.prototype.setProvince = function (province) {
    this.province = province;
};

Person.prototype.getProvince = function () {
    return this.province;
};

Person.prototype.setDistrict = function (district) {
    this.district = district;
};

Person.prototype.getDistrict = function () {
    return this.district;
};

Person.prototype.setAddress = function (address) {
    this.address = address;
};

Person.prototype.getAddress = function () {
    return this.address;
};
