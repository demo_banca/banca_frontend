/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var provinces = new Array();
    var districts = new Array();

    $('#datePickerBirthdate')
            .datepicker({
                format: 'yyyy/mm/dd'
            })
            .on('changeDate', function (e) {
                // Revalidate the date field
                $('#formNewPerson').formValidation('revalidateField', 'birthdate');
            });

    $("#btnCancel").click(function () {
        $("#formNewPerson").data("formValidation").resetForm();
        clearForm();
    });

    $("#btnSave").click(function () {
        $("#formNewPerson").formValidation("validate");
    });

    $("#cboDepartment").change(function () {
        var departament = $("#cboDepartment").val();

        if (departament === "") {
            $("#cboProvince").empty();
            $("#cboDistrict").empty();
            $("#cboProvince").data("combobox").reset();
            $("#cboDistrict").data("combobox").reset();
        } else {

            $("#cboProvince").append($("<option>", {value: "", text: ""}, "</option>"));
            var cont = 0;

            $.each(provinces, function (index, itemData) {
                if (departament === itemData[0]) {
                    cont++;
                    $("#cboProvince").append($("<option>", {value: itemData[1], text: itemData[1]}, "</option>"));
                }
            });

            if (cont === 0) {
                $("#cboProvince").empty();
                $("#cboDistrict").empty();
            }
            
            $("#formNewPerson").formValidation("revalidateField", "province");
            $("#formNewPerson").formValidation("revalidateField", "district");
            
        }

        $("#cboProvince").data("combobox").refresh();
        $("#cboDistrict").data("combobox").refresh();
    });

    $("#cboProvince").change(function () {
        var province = $("#cboProvince").val();

        if (province === "") {
            $("#cboDistrict").empty();
            $("#cboDistrict").data("combobox").reset();
        } else {

            $("#cboDistrict").append($("<option>", {value: "", text: ""}, "</option>"));
            var cont = 0;

            $.each(districts, function (index, itemData) {
                if (province === itemData[0]) {
                    cont++;
                    $("#cboDistrict").append($("<option>", {value: itemData[1], text: itemData[1]}, "</option>"));
                }
            });

            if (cont === 0) {
                $("#cboDistrict").empty();
            } 
            
            $("#formNewPerson").formValidation("revalidateField", "district");
            
        }

        $("#cboDistrict").data("combobox").refresh();
    });


    getAllDepartmentsInvoker();

    function getAllDepartmentsInvoker() {

        $("#cboDepartment").append($("<option>", {value: "", text: ""}, "</option>"));
        $("#cboProvince").append($("<option>", {value: "", text: ""}, "</option>"));
        $("#cboDistrict").append($("<option>", {value: "", text: ""}, "</option>"));

        getAllDepartments()
                .done(function (data, textStatus, XMLHttpRequest) {

                    $.each(data, function (index1, itemData1) {
                        $("#cboDepartment").append($("<option>", {value: itemData1.name, text: itemData1.name}, "</option>"));

                        $.each(itemData1.provinces, function (index2, itemData2) {
                            provinces.push(new Array(itemData1.name, itemData2.name));

                            $.each(itemData2.districts, function (index3, itemData3) {
                                districts.push(new Array(itemData2.name, itemData3));
                            });
                        });
                    });

                    $("#cboDepartment").data("combobox").refresh();
                    $("#cboProvince").data("combobox").refresh();
                    $("#cboDistrict").data("combobox").refresh();
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    notification(NOTIFICATION_WARNING, "Error in list of departments");
                    console.log(XMLHttpRequest.getAllResponseHeaders());
                    console.log(textStatus);
                    console.log(errorThrown);
                });
    }

    function saveForm() {

        var name = $("#txtName").val();
        var birthdate = $("#txtBirthdate").val();
        birthdate = new Date(birthdate).getTime();
        var department = $("#cboDepartment").val();
        var province = $("#cboProvince").val();
        var district = $("#cboDistrict").val();
        var address = $("#txtAddress").val();

        var person = new Person();
        person.setName(name);
        person.setBirthdate(birthdate);
        person.setDepartment(department);
        person.setProvince(province);
        person.setDistrict(district);
        person.setAddress(address);

        createNewPerson(person)
                .done(function (data, textStatus, XMLHttpRequest) {
                    if (data) {
                        $("#formNewPerson").data("formValidation").resetForm();
                        clearForm();
                        notification(NOTIFICATION_SUCCESS, "Person added successfully!");
                    } else {
                        notification(NOTIFICATION_WARNING, "Error Person!");
                    }
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.getAllResponseHeaders());
                    console.log(textStatus);
                    console.log(errorThrown);
                });
    }


    function clearForm() {
        $("#txtName").val("");
        $("#txtBirthdate").val("");
        $("#cboDepartment").data("combobox").reset();
        $("#cboProvince").data("combobox").reset();
        $("#cboDistrict").data("combobox").reset();
        $("#txtAddress").val("");
    }

    $("#formNewPerson")
            .formValidation({
                framework: "bootstrap",
                icon: {
                    valid: "glyphicon glyphicon-ok",
                    invalid: "glyphicon glyphicon-remove",
                    validating: "glyphicon glyphicon-refresh"
                },
                excluded: ":disabled",
                fields: {
                    name: {
                        row: ".col-xs-6",
                        validators: {
                            notEmpty: {
                                message: "The username is required"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9@-_ \.]+$/,
                                message: "The input character is not allowed."
                            }
                        }
                    },
                    birthdate: {
                        row: ".col-xs-6",
                        validators: {
                            notEmpty: {
                                message: "The birthdate is required"
                            },
                            date: {
                                format: "YYYY/MM/DD",
                                max: new Date(),
                                message: "The birthdate is not a valid"
                            }
                        }
                    },
                    department: {
                        row: ".col-xs-4",
                        validators: {
                            notEmpty: {
                                message: "The department is required"
                            }
                        }
                    },
                    province: {
                        row: ".col-xs-4",
                        validators: {
                            notEmpty: {
                                message: "The province is required"
                            }
                        }
                    },
                    district: {
                        row: ".col-xs-4",
                        validators: {
                            notEmpty: {
                                message: "The district is required"
                            }
                        }
                    },
                    address: {
                        validators: {
                            notEmpty: {
                                message: "The address is required"
                            }
                        }
                    }
                }
            })
            // Using Bootbox for department, province and district select elements
            .find("[name='department'], [name='province'], [name='district']").combobox().end()
            .on("success.field.fv", function (e, data) {
                /* REMOVE ICON OK*/
                var $parent1 = data.element.parents(".form-group");
                var $parent2 = data.element.parents(".col-xs-6");
                var $parent3 = data.element.parents(".col-xs-4");

                // Remove the has-success class
                $parent1.removeClass("has-success");
                $parent2.removeClass("has-success");
                $parent3.removeClass("has-success");

                // Hide the success icon
                data.element.data("fv.icon").hide();
            })
            .on("err.form.fv", function (e, data) {
                notification(NOTIFICATION_WARNING, "Error!");
            })
            .on("success.form.fv", function (e, data) {
                saveForm();
            });

});